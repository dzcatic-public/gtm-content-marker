<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/dzcatic-public/gtm-content-marker/
 * @since      1.0.0
 *
 * @package    Content_Marker
 * @subpackage Content_Marker/admin/partials
 */
?> 
<nav class="navbar navbar-light bg-light">
  <a class="navbar-brand" href="#">
    <img src="https://api.iconify.design/twemoji:letter-g.svg" width="30" height="30" class="d-inline-block align-top" alt="">
    GTM Content Marker
  </a>
</nav>
<hr>
<div id="content_marker_settings">
    <form action="options.php" method="post">
        <div class="container">
            <div class="row">
                <div class="input-group mb-6">
                    <?php
                        settings_fields( 'content_marker_section' );
                        do_settings_fields( 'content_marker_page', 'content_marker_section' );
                    ?>
                    <div class="input-group-append">  
                        <input type="submit" name="submit" id="submit" class="button button-primary" value="Save"  /> 
                    </div>
                </div>
            </div>
        </div> 
    </form>
</div>
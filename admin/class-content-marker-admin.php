<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://bitbucket.org/dzcatic-public/gtm-content-marker/
 * @since      1.0.0
 *
 * @package    Content_Marker
 * @subpackage Content_Marker/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Content_Marker
 * @subpackage Content_Marker/admin
 * @author     Dzenad Catic <dzcatic@gmail.com>
 */
class Content_Marker_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		// Disable Curly Quotes, It's need for support readable Qoutes
		remove_filter('the_content', 'wptexturize');
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function admin_enqueue_scripts() {

		wp_enqueue_style( 'bbundle', plugin_dir_url( __FILE__ ) . 'css/bbundle.css', array(), '', 'all' );
		wp_enqueue_script( 'bbundle', plugin_dir_url( __FILE__ ) . 'js/bbundle.js', array( 'jquery' ), '', true );

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/content-marker-admin.css', array(), $this->version, 'all' );

	}

	// Public scripts here
	public function public_enqueue_scripts() {
		wp_enqueue_style( 'bbundle', plugin_dir_url( __FILE__ ) . 'css/bbundle.css', array(), '', 'all' );
		wp_enqueue_script( 'bbundle', plugin_dir_url( __FILE__ ) . 'js/bbundle.js', array( 'jquery' ), '', false );
		// Plugin js file
		wp_enqueue_script( 'content-marker-admin', plugin_dir_url( __FILE__ ) . 'js/content-marker-admin.js', array( 'jquery' ), '', false );
	}

	// Menupage callback
	function content_marker_menu_register(){
		add_menu_page( "GTM Content Marker", "GTM Content Marker", "manage_options", "content-marker-opt", [$this,'content_marker_menu_callback'], 'https://api.iconify.design/twemoji:letter-g.svg',45 );
		// Registering setting API for content marker section
		add_settings_section( 'content_marker_section', '', '', 'content_marker_page' );
		$this->content_marker_input_fields();
	}

	/**
	 * SECTION NAME: content_marker_section
	 * PAGE NAME: content_marker_page
	 * @package All admin inputs will be here
	 */
	function content_marker_input_fields(){
		//Text field for adding texts to display
		add_settings_field( 'content_marker_mark_texts', ' ', [$this,'content_marker_mark_texts_func'], 'content_marker_page', 'content_marker_section');
		register_setting( 'content_marker_section', 'content_marker_mark_texts'); 
	}

	/**
	 * @package Text field for adding texts to display
	 */
	function content_marker_mark_texts_func(){
		echo '<input type="text" placeholder="Enter text..." class="form-control" name="content_marker_mark_texts" id="content_marker_mark_texts" value="'.get_option( 'content_marker_mark_texts' ).'">';
	}

	/**
	 * Replace post contents with our customised value
	 */
	function content_marker_marking_process($the_content){
		$markable_text = stripcslashes(get_option( 'content_marker_mark_texts' ));
		$the_content = str_replace($markable_text, '<span style="font-weight: bold !important" class="content_marker_texts">'.$markable_text.'</span>',$the_content);
		// wpautop print's htmlspecialchars to html view
		return wpautop($the_content);
	}

	/**
	 * Menu page display
	 */
	function content_marker_menu_callback(){
		require_once plugin_dir_path( __FILE__ )."partials/content-marker-admin-display.php";
	}

}

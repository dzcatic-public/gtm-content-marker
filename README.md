---

## GTM Content Marker
Wordpress plugin 
Contributors: Dzenad Catic  
Requires at least: 5.1
Tested up to: 5.7.2
Requires PHP: 7.2 
License: GNU GENERAL PUBLIC LICENSE
License URI: http://www.gnu.org/licenses/gpl-2.0.html

---
## Plugin Description 
 
Plugin is used to mark words in the post as bold.

Plugin Backend:
In the plugin page the user should be able see a input text field and a save button.

The text input should contain the text that was previously saved, or be blank if there was no previously saved data.

Frontend:
If the text input is blank, nothing unusual should take place.

If the text input is not blank, all the substrings in the content of the post that are exactly matched with the text input are displayed as bold. 

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. 

## How to install plugin
1. Download plugin : https://drive.google.com/file/d/1bQ2n6cjnE8zcxHOmaaLrVSunCcXLBZA9/view?usp=sharing 
2  Click on Plugins -> Add New -> Upload -> Select plugin -> Install
3. After installation click on Activate plugin  

## Screenshoots 
https://drive.google.com/drive/folders/1Lrd4mCuXSqt3dh83VYNbcMf2FrrgFPQO?usp=sharing
 

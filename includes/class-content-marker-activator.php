<?php

/**
 * Fired during plugin activation
 *
 * @link       https://bitbucket.org/dzcatic-public/gtm-content-marker/
 * @since      1.0.0
 *
 * @package    Content_Marker
 * @subpackage Content_Marker/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Content_Marker
 * @subpackage Content_Marker/includes
 * @author     Dzenad Catic <dzcatic@gmail.com>
 */
class Content_Marker_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}

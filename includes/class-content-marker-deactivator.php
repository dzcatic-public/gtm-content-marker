<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://bitbucket.org/dzcatic-public/gtm-content-marker/
 * @since      1.0.0
 *
 * @package    Content_Marker
 * @subpackage Content_Marker/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Content_Marker
 * @subpackage Content_Marker/includes
 * @author     Dzenad Catic <dzcatic@gmail.com>
 */
class Content_Marker_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}

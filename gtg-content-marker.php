<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://example.com
 * @since             1.0.0
 * @package           Content_Marker
 *
 * @wordpress-plugin
 * Plugin Name:       GTM Content Marker
 * Plugin URI:        https://bitbucket.org/dzcatic-public/gtm-content-marker/
 * Description:       Enter text inside the pluging. Entered text will be marked in post as bold.
 * Version:           1.0.0
 * Author:            Content Marker
 * Author URI:        https://bitbucket.org/dzcatic-public/gtm-content-marker/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       content-marker
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CONTENT_MARKER_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-content-marker-activator.php
 */
function activate_content_marker() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-content-marker-activator.php';
	Content_Marker_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-content-marker-deactivator.php
 */
function deactivate_content_marker() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-content-marker-deactivator.php';
	Content_Marker_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_content_marker' );
register_deactivation_hook( __FILE__, 'deactivate_content_marker' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-content-marker.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_content_marker() {

	$plugin = new Content_Marker();
	$plugin->run();

}
run_content_marker();
